#from instabot import Bot
import time
import os
from pathlib import Path
import json
import urllib
import ssl
import traceback


ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

ssl._create_default_https_context = ssl._create_unverified_context

f = open("data_uuids.json")
DATA_UUIDS_OBJECT = json.load(f)
f.close()
f = open("bituh_data.json")
DATA_BITUH_OBJECT = json.load(f)["DATA"]
f.close()
f = open("more_uuids.json")
DATA_MORE_OBJECT = json.load(f)["DATA"]
f.close()

def get_more_details(uuid):

    url = "https://www.izkor.gov.il/search/memory/presentation/" + uuid
    r = urllib.request.urlopen(url)
    data = json.loads(r.read().decode(r.info().get_param('charset') or 'utf-8'))
    return data["data"]

def check_if_uploaded(obj):
    f = open("uploaded_to_instagram.json")
    UPLOADED_ITEMS = json.load(f)
    f.close()
    uploaded_item_arr = UPLOADED_ITEMS["uploaded"]
    
    for uploaded_obj in uploaded_item_arr:
        if 'uuid' in list(obj.keys()) and 'uuid' in list(uploaded_obj.keys()): # izkor website data
            if obj['uuid'] == uploaded_obj['uuid']:
                return True
        if 'about' in list(obj.keys()) and 'about' in list(uploaded_obj.keys()): # bituh website data
            if obj['about'] == uploaded_obj['about']:
                return True
        if 'place' in list(obj.keys()) and 'place' in list(uploaded_obj.keys()): # more xlsx data
            if obj['full_name'] == uploaded_obj['full_name'] and obj['place'] == uploaded_obj['place']:
                return True
        
    return False

def update_as_uploaded(obj):
    f = open("uploaded_to_instagram.json")
    UPLOADED_ITEMS = json.load(f)
    f.close()
    
    UPLOADED_ITEMS["uploaded"].append(obj)
    
    f = open("uploaded_to_instagram.json", "w+")
    f.write(json.dumps(UPLOADED_ITEMS))
    f.close()
    
    
            # jsonFile = open("uploaded_to_instagram.json", "w+")
            # jsonFile.write(json.dumps(UPLOADED_ITEMS))
            # jsonFile.close()

from instagrapi import Client

cl = Client()
cl.login("lezichram1@gmail.com", "leziCHRAM6276123!")
#cl.login("lezichram1@gmail.com", "leziCHRAM6276123!")
#cl.login("israelisraeli6558","Aa123456")
#print(cl.account_info().dict())

# bot=Bot()
# bot.login(username="lezichram1@gmail.com",password="leziCHRAM62761234")
#bot.login(username="israelisraeli6558",password="Aa123456")
time.sleep(0.2)

years = range(1800,1955)

for year in years:
    
    try:
        if os.path.isdir("results/" + str(year)):
            results = os.listdir("results/" + str(year) + "/")
            
            for item in results:
                if os.path.isdir("results/" + str(year) + "/" + str(item)):
                    
                    photo = Path("results/" + str(year) + "/" + str(item) + "/" + str(item) + ".jpg")
                    if photo.exists():
                        print(photo)
                        if item in DATA_UUIDS_OBJECT:
                            
                            obj = get_more_details(item)
                            
                            full_name = DATA_UUIDS_OBJECT[item]["full_name"]
                            shortname = full_name.replace(" ", '').replace("-", '').replace("'", '').replace("\"", '').replace("(", '').replace(")", '').replace("&", '').replace(",", '')
                            hebrew_date = obj["death_date_hebrew"]
                            date = obj["death_date"].replace("-",".")
                            link = f"""https://www.izkor.gov.il/-/{item}"""
                            caption = f"""
                            {full_name} ז״ל, נפל ביום {hebrew_date} ({date})
                            
                            #{shortname}זל #לזכרם #יוםהזיכרון #יוםהזיכרון2023 #חללזכרונות #יוםהזכרוןהתשפג #lezichram #YomHazikaron #IsraelRemembers #memorialday
                            """
                            
                            if not check_if_uploaded(DATA_UUIDS_OBJECT[item]):  
                                print(caption)
                                #t = bot.upload_photo(photo,caption=caption)
                                #print("ANSWER FROM BOT UPLOAD: ")
                                #print(t)
                                #if t and 'comment_likes_enabled' in list(t.keys()):
                                #if not t["comment_likes_enabled"]:
                                        # uploaded
                                
                                media = cl.photo_upload(
                                    photo,
                                    caption,
                                )

                                print(media.dict())
                                print("updating object as uploaded: ")
                                print(DATA_UUIDS_OBJECT[item])
                                update_as_uploaded(DATA_UUIDS_OBJECT[item])
                                    
                                time.sleep(0.5)
    except Exception as e:
        print(str(e))
        traceback.print_exc()
    
    try:
        if os.path.isdir("results/bituh/" + str(year)):
            print("bituh: " + str(year))
            bituh_results = list(filter(lambda x: '.jpg' in x, os.listdir("results/bituh/" + str(year) + "/")))
        
            for item in bituh_results:
                photo = Path("results/bituh/" + str(year) + "/" + item)
                if photo.exists():
                    print(photo)
                    temp = item.split("_")
                    name = temp[0] + " " + temp[1]
                    
                    try:
                        obj = list(filter(lambda x: name in x["full_name"] and str(year) == str(x["year"]) ,DATA_BITUH_OBJECT))[0]
                    
                        
                        full_name = obj["full_name"]
                        try:
                            #shortname = obj["full_name"].split("")[0].replace(" ", '')[:-1]
                            name_parts = obj["full_name"].split(" ")
                            shortname = ""
                            if len(name_parts) >= 5 and name_parts[4] == "ז״ל":
                                shortname = name_parts[0] + " " + name_parts[1] + " " + name_parts[2] + " " + name_parts[3]
                            elif len(name_parts) >= 4 and name_parts[3] == "ז״ל":
                                shortname = name_parts[0] + " " + name_parts[1] + " " + name_parts[2]
                            elif len(name_parts) >= 3 and name_parts[2] == "ז״ל":
                                shortname = name_parts[0] + " " + name_parts[1]
                            else:
                                shortname = name
                            
                            shortname = shortname.replace(" ", '').replace("-", '').replace("'", '').replace("\"", '').replace("(", '').replace(")", '').replace("&", '').replace(",", '')
                        except:
                            shortname = full_name
                        date = obj["year"]
                        #link = f"""https://www.izkor.gov.il/-/{item}"""
                        caption = f"""
                        {full_name}, נפל בתאריך {date}
                        
                        #{shortname}זל #לזכרם #יוםהזיכרון #יוםהזיכרון2023 #חללזכרונות #יוםהזכרוןהתשפג #lezichram #YomHazikaron #IsraelRemembers #memorialday
                        """
                        
                        if not check_if_uploaded(obj):  
                            print(caption)
                            
                            #t = bot.upload_photo(photo,caption=caption)
                            #if t and 'comment_likes_enabled' in list(t.keys()):
                            #    if not t["comment_likes_enabled"]:
                                    # uploaded
                            media = cl.photo_upload(
                                    photo,
                                    caption,
                                )

                            print(media.dict())
                            update_as_uploaded(obj)
                            time.sleep(0.5)
                            
                    except Exception as e:
                        print(f"""Error: {str(e)}""")
                        traceback.print_exc()
                        
    except Exception as e:
        traceback.print_exc()
        
    try:
        if os.path.isdir("results/more_xl/" + str(year)):
            more_results = list(filter(lambda x: '.jpg' in x, os.listdir("results/more_xl/" + str(year) + "/")))
        
            for item in more_results:
                photo = Path("results/more_xl/" + str(year) + "/" + item)
                if photo.exists():
                    print(photo)
                    temp = item.split("_")
                    name = temp[0] + " " + temp[1]
                    
                    try:
                        obj = list(filter(lambda x: name in x["full_name"] and str(year) == str(x["year"]) ,DATA_MORE_OBJECT))[0]
                            
                        full_name = obj["full_name"]
                        try:
                            #shortname = obj["full_name"].split("")[0].replace(" ", '')[:-1]
                            shortname = full_name.replace(" ", '').replace("-", '').replace("'", '').replace("\"", '').replace("(", '').replace(")", '').replace("&", '').replace(",", '')
                        except:
                            shortname = full_name
                        
                        hebrew_date = obj["hebrew_date"]
                        date = obj["date"]
                        #link = f"""https://www.izkor.gov.il/-/{item}"""
                        caption = f"""
                        {full_name}, נפל בתאריך {hebrew_date} ({date}).
                        
                        #{shortname}זל #לזכרם #יוםהזיכרון #יוםהזיכרון2023 #חללזכרונות #יוםהזכרוןהתשפג #lezichram #YomHazikaron #IsraelRemembers #memorialday
                        """
                        
                        if not check_if_uploaded(obj):  
                            print(caption)
                            #t = bot.upload_photo(photo,caption=caption)
                            #if t and 'comment_likes_enabled' in list(t.keys()):
                            #    if not t["comment_likes_enabled"]:
                                    # uploaded
                            media = cl.photo_upload(
                                photo,
                                caption,
                            )

                            print(media.dict())
                            update_as_uploaded(obj)
                            time.sleep(0.5)
                    except:
                        print("not found object for: " + name + " " + year)
                        
                        
    except Exception as e:
        traceback.print_exc()