
from PIL import Image, ImageFont, ImageDraw, ImageOps
font_type_1 = ImageFont.truetype("Rubik-Regular.ttf", 47, layout_engine=ImageFont.Layout.RAQM)
font_type_1_sm = ImageFont.truetype("Rubik-Regular.ttf", 40, layout_engine=ImageFont.Layout.RAQM)
font_type_2 = ImageFont.truetype("Rubik-Regular.ttf", 54, layout_engine=ImageFont.Layout.RAQM)
font_type_2_sm = ImageFont.truetype("Rubik-Regular.ttf", 49, layout_engine=ImageFont.Layout.RAQM)
font_type_3 = ImageFont.truetype("Rubik-Regular.ttf", 32, layout_engine=ImageFont.Layout.RAQM)

import json
import urllib.request
import multiprocessing
import requests
import time

import textwrap
import ssl
import os
import re
import datetime

from selenium import webdriver 
from selenium.webdriver.chrome.service import Service as ChromeService 
from webdriver_manager.chrome import ChromeDriverManager 

from selenium.webdriver.common.by import By  
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

ssl._create_default_https_context = ssl._create_unverified_context

def download_person_image(imgsrc,name):
    print("Downloading image for name: " + name)
    # urllib.request.urlretrieve(
    # f"""{imgsrc}""",
    # f"""{name}.jpg""")
    
    file_id = '1Rww0AAxlxv27zhw0FewHVEKDUD4I6s4U'
    destination = '1Rww0AAxlxv27zhw0FewHVEKDUD4I6s4U.jpeg'
    download_file_from_google_drive(imgsrc, imgsrc + '.jpeg')
    
    return ImageOps.grayscale(Image.open(f"""{imgsrc}.jpeg"""))

def download_file_from_google_drive(id, destination):
    URL = "https://docs.google.com/uc?export=download"

    session = requests.Session()

    response = session.get(URL, params = { 'id' : id }, stream = True)
    token = get_confirm_token(response)

    if token:
        params = { 'id' : id, 'confirm' : token }
        response = session.get(URL, params = params, stream = True)

    save_response_content(response, destination)    

def get_confirm_token(response):
    for key, value in response.cookies.items():
        if key.startswith('download_warning'):
            return value

    return None

def save_response_content(response, destination):
    CHUNK_SIZE = 32768

    with open(destination, "wb") as f:
        for chunk in response.iter_content(CHUNK_SIZE):
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)


def create_image(obj):
    folder_name = '_'.join(obj["full_name"].split(" ")[:2])
    gender = obj["gender"]
    
    baseimage = "female.png"
    if gender == "ז":
        baseimage = "male.png"
    print("Choosed baseimage: " + baseimage)
    
        
    #The regex pattern that we created
    # pattern = "[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}"

    # #Will return all the strings that are matched
    # dates = re.findall(pattern, obj["about"])
    
    # found_date = ""
    # print(f"""dates: {dates}""")
    # for date in dates:
    #     if "-" in date:
    #         day, month, year = map(int, date.split("-"))
    #     else:
    #         day, month, year = map(int, date.split("/"))
    #     if 1 <= day <= 31 and 1 <= month <= 12:
    #         print(date)
    #         found_date = date
        
    # print(f"""found_date: {found_date}""")
    # print(side_details)
    
    # found_year = ""
    # if found_date != "":
    #     found_year = found_date.split('/')[2]
        
    with Image.open(baseimage) as image: 

        width, height = image.size 
        
        if obj["img_src"] == "nan" or obj["img_src"] == None:
            print("No image, choosed default..")
            person = Image.open("no_image_default.jpeg")     
        else:
            print(obj["img_src"])
            #https://drive.google.com/open?id=1Rww0AAxlxv27zhw0FewHVEKDUD4I6s4U 
            #https://drive.google.com/u/0/uc?id=1Rww0AAxlxv27zhw0FewHVEKDUD4I6s4U&export=download
            #downloadable_link = obj["img_src"].replace('open','u/0/uc') + "&export=download"
            #print("downloadable_link: " + downloadable_link)
            image_id = str(obj["img_src"]).split("?id=")[1]
            person = download_person_image(image_id, folder_name)
            time.sleep(2)
        
        width, height = person.size  
        
        correct_width = 300
        ratio = height / width
        person = person.resize((correct_width, int(correct_width * ratio))) 
        image.paste(person, (670, 140))   
        
        draw = ImageDraw.Draw(image)
        
        if obj["rank"] != None:
            rank_text = obj["rank"]
            _width, _height = font_type_1.getsize(rank_text)
            draw.text((630 - _width, 150),rank_text,(255,255,255),font=font_type_1)
        
            
        name_text = obj["full_name"]
        _width, _height = font_type_2.getsize(name_text)
        f = font_type_2
        if _width >= 550:
            _width, _height = font_type_2_sm.getsize(name_text)
            f = font_type_2_sm
        draw.text((630 - _width, 150+64),name_text,(255,255,255),direction='rtl',align='right',features='rtla',font=f,stroke_width=1,stroke_fill="white")
        
        # gender_prefix = "בן" if obj["gender"] == "ז" else "בת"
        
        # if obj["mother_name"] or obj["father_name"]:
        #     if obj["mother_name"] != None and obj["mother_name"] == None:
        #         parents_text = f"""{gender_prefix} {obj["mother_name"]}"""
        #     elif obj["mother_name"] == None and obj["mother_name"] != None:
        #         parents_text = f"""{gender_prefix} {obj["father_name"]}"""
        #     else:
        #         parents_text = f"""{gender_prefix} {obj["mother_name"]} ו{obj["father_name"]}"""
        
        offset = "75"
        if len(obj["parents"]) > 0:
            parents_text = obj["parents"]
            if gender == 'ז':
                parents_text = "בן " + parents_text
            else:
                parents_text = "בת " + parents_text
                
            _width, _height = font_type_1.getsize(parents_text)
            f2 = font_type_1
            if _width >= 550:
                _width, _height = font_type_2_sm.getsize(name_text)
                f2 = font_type_1_sm
            draw.text((630 - _width, 215+64),parents_text,(255,255,255),font=f2)
            offset = 0
            
        side_details = f"""נפל ביום {obj["hebrew_data"]} ({obj["date"]})
מקום מנוחתו {obj["place"]}""".splitlines()
        
        
        line1_text = side_details[0]
        _width, _height = font_type_3.getsize(line1_text)
        draw.text((630 - _width, 290-offset+64),line1_text,(255,255,255),font=font_type_3)
        
        if len(side_details) >= 2:
            line2_text = side_details[1]
            _width, _height = font_type_3.getsize(line2_text)
            draw.text((630 - _width, 330-offset+64),line2_text,(255,255,255),font=font_type_3)
            
            if len(side_details) >= 3:
                line3_text = side_details[2]
                _width, _height = font_type_3.getsize(line3_text)
                draw.text((630 - _width, 370-offset+64),line3_text,(255,255,255),font=font_type_3)
                
                if len(side_details) >= 4:
                    line4_text = side_details[3]
                    _width, _height = font_type_3.getsize(line4_text)
                    draw.text((630 - _width, 410-offset+64),line4_text,(255,255,255),font=font_type_3)
                    
                    if len(side_details) >= 5:
                        line5_text = side_details[4]
                        _width, _height = font_type_3.getsize(line5_text)
                        draw.text((630 - _width, 450-offset+64),line5_text,(255,255,255),font=font_type_3)
                
        
        rgb_im = image.convert('RGB')
        
        
        if not os.path.isdir("results/more_xl/" + str(obj["year"])):
            os.makedirs("results/more_xl/" + str(obj["year"]))   
        
        rgb_im.save('results/more_xl/' + str(obj["year"]) + "/" + folder_name+"_"+str(obj["year"])+'.jpg')
        
        #os.remove(f"""{folder_name+"_"+str(obj["year"])}.jpeg""")

            
def proccess(item):
    #folder_name = ''.join(item["full_name"].split(" ")[:2])
    # print(folder_name)
    # if not os.path.isdir("results/bituh/" + folder_name):
    #     os.makedirs("results/bituh/" + folder_name)     
        
    try:
        create_image(item)
    except Exception as e:
        print(str(e))
    
    # https://drive.google.com/open?id=1Rww0AAxlxv27zhw0FewHVEKDUD4I6s4U 
    # https://drive.google.com/u/0/uc?id=1Rww0AAxlxv27zhw0FewHVEKDUD4I6s4U&export=download
    
    
def main():
    f = open("more_uuids.json")
    data = json.load(f)
    f.close()
    DATA = data["DATA"]
    
    for item in DATA:
        print(item)
        year = item["year"]
        
        if not os.path.isdir("results/more_xl/" + year):
            os.makedirs("results/more_xl/" + year)
            
        create_image(item)
                
    # pool_obj = multiprocessing.Pool()
    # pool_obj.map(proccess,DATA)
    # pool_obj.close()

            
    
if __name__ == "__main__":
    main()

