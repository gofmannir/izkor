# import json
# from urllib import request, parse
# import ssl
# import requests

# f = open("data_uuids.json")
# JSONFILE_OBJECT = json.load(f)

# f.close()

# print(JSONFILE_OBJECT["en_974958287c8d12c2ef98e331234d34ba"]["full_name"])

import pandas as pd

import json
import datetime

f = open("data_uuids.json")
data = json.load(f)
f.close()
f = open("bituh_data.json")
data_bituh = json.load(f)["DATA"]
f.close()

file_errors_location = 'more.xlsx'
df = pd.read_excel(file_errors_location)

records = df.to_dict('records')

f = open("more_uuids.json")
JSONFILE_OBJECT = json.load(f)
DATA = JSONFILE_OBJECT
f.close()


for item in records:
    has = False
    for halal in data:
        if item["שם מלא"] in data[halal]["full_name"]:
            has = True
            
    for nofel in data_bituh:
        if item["שם מלא"] in nofel["full_name"]:
            has = True
            
    if item["שם מלא"] == "ישראל ישראלי":
        has = True
    
    if not has:
        full_name = item["שם מלא"]
        rank = item["דרגה (אם נפל/ה בפעולת איבה כתבו - נרצח/ה בפעולת איבה)"]
        parents = item["שמות ההורים"]
        hebrew_data = item["תאריך פטירה עברי"]
        date = item["תאריך פטירה לועזי"]
        date = date if type(date) == str else date.strftime('%d/%m/%Y')
        place = item["בית עלמין"]
        gender = item["זכר/נקבה"]
        img_src = item["תמונה"]
        if not img_src:
            img_src = ""
            
        found_year = ""
        if date != "":
            found_year = date.split('/')[2]
        
        DATA["DATA"].append({
            "full_name": full_name,
            "rank": rank,
            "gender": "ז" if gender == "זכר" else 'נ',
            "parents": parents,
            "hebrew_data": hebrew_data,
            "date":date,
            "year": found_year,
            "place":place,
            "img_src": str(img_src)
        })
    
print(DATA)
jsonFile = open("more_uuids.json", "w+")
jsonFile.write(json.dumps(DATA))
jsonFile.close()
    
    
    
    